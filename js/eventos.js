var eventos=(function(){
    var _evento = function() {
        var buscar= document.getElementById("buscar");
        buscar.addEventListener("click", function(obj_evento){
            var busca=document.getElementById("texto_busca").value;
            if(busca!=""&&busca!=undefined)
            youtube.llenaMapa();
        },false);

        var prevPag= document.getElementById("prev-pag");
        var sigPag= document.getElementById("sig-pag");
        prevPag.addEventListener("click",function(){
            sigPag.style.display = "block";
            var actual=parseInt(prevPag.getAttribute("actual"));
            youtube.muestraBusquedas(actual-20,actual-10);
            prevPag.setAttribute("actual",actual-10);
            sigPag.setAttribute("actual",actual-10);
            if(actual-10<=10){
                prevPag.style.display = "none";
            }
        },false);
        sigPag.addEventListener("click",function(){
            prevPag.style.display = "block";
            var actual=parseInt(prevPag.getAttribute("actual"));
            var total=document.getElementById("numero_resultados").value;
            if(actual+10<total){
                youtube.muestraBusquedas(actual,actual+10);
                prevPag.setAttribute("actual",actual+10);
                sigPag.setAttribute("actual",actual+10);
            }else{
                console.log("desaparece sig")
                youtube.muestraBusquedas(actual,total);
                prevPag.setAttribute("actual",actual+10);
                sigPag.style.display = "none";
            }
            },false);
        
        var cancelar= document.getElementById("cancelar");
        var enviar=document.getElementById("enviar_correo");
        cancelar.addEventListener("click", function(){
            var divcorreo= document.getElementById("envio_de_mensaje");
            divcorreo.style.display="none";
        },false);
        enviar.addEventListener("click",function(){
            var id_video=document.getElementById("envio_de_mensaje").getAttribute("idvideo");
            console.log(document.getElementById("correo-destino"));
              var correo=document.getElementById("correo-destino").value;
              var asunto=document.getElementById("asunto_del_correo").value;
              if(correo!=""&&correo!=undefined&&asunto!=""&&asunto!=undefined){
              gmail.sendMessage(
            {
              'To': correo,
              'Subject': asunto
            },
            "https://www.youtube.com/embed/"+id_video +" "+ "https://www.youtube.com/watch?v="+id_video,
            gmail.composeTidy
          );
          var divcorreo= document.getElementById("envio_de_mensaje");
          divcorreo.style.display="none";
          document.getElementById("correo-destino").value="";
          document.getElementById("asunto_del_correo").value="";
        }
            
        },false);

    };
    var _iniciar_eventos = function(){
        _evento();
    };
    
    return {
        "iniciar_eventos": _iniciar_eventos
    };

})();

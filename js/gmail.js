var gmail=(function(){
    var _getMessages = function(idLabel) {
        var tab=document.getElementById("tabla");
        var cuerpo=tab.lastElementChild;
        tab.removeChild(cuerpo);
        cuerpo=document.createElement("tbody");
        cuerpo.setAttribute("id","cuerpo");
        tab.appendChild(cuerpo);
        
        gapi.client.gmail.users.messages.list({
        "userId": "me",
        "labelIds": idLabel
        })
        .then(function(response) {
                _listMessages(response);
              },
              function(err) { console.error("Execute error", err); });
    };

    var _getOneMessage = function(id,i) {
        return gapi.client.gmail.users.messages.get({
          "userId": "me",
          "id": id,
          "metadataHeaders": null
        })
            .then(function(response) {
                    //console.log(response)
                    var cuerpo=document.getElementById("cuerpo");
                    var tr= document.createElement("tr");
                    var td=document.createElement("td");
                    td.textContent=response.headers.date;
                    tr.appendChild(td);
                    td=document.createElement("td");
                    td.textContent= response.result.snippet;
                    tr.appendChild(td);
                    var bot= document.createElement("input");
                    bot.setAttribute("type","button");
                    bot.setAttribute("value","ELIMINAR");
                    bot.setAttribute("id",id);
                    td=document.createElement("td");
                    td.appendChild(bot);
                    tr.appendChild(td);
                    cuerpo.appendChild(tr);
                  },
                  function(err) { console.error("Execute error", err); });
      }

    var _listMessages = function(response){
        var mensajes=response.result.messages;
        if(mensajes!=undefined){
        for(var i=0; i< mensajes.length; i++){
          _getOneMessage(mensajes[i].id,i);
        }
      }
    };

    var _deleteMessage = function(id) {
          return gapi.client.gmail.users.messages.delete({
              "userId": "me",
              "id": id
          })
          .then(function(response) {
              // Handle the results here (response.result has the parsed body).
              console.log("Response", response);
              _getMessages();
            },
            function(err) { console.error("Execute error", err); });
    };
    var _sendEmail= function(){
        var destino=document.getElementById("destino").value;
        var asunto=document.getElementById("asunto").value;
        var mensaje=document.getElementById("mensaje").value;
        _sendMessage(
          {
            'To': destino,
            'Subject': asunto
          },
          mensaje,
          _composeTidy
        );
        return false;
      };
      
    var _sendMessage = function(headers_obj, message, callback){
        var email = '';
        for(var header in headers_obj)
          email += header += ": "+headers_obj[header]+"\r\n";

        email += "\r\n" + message;
        var sendRequest = gapi.client.gmail.users.messages.send({
          'userId': 'me',
          'resource': {
            'raw': window.btoa(email).replace(/\+/g, '-').replace(/\//g, '_')
          }
        });
        return sendRequest.execute(callback);
      };
      
      var _composeTidy = function()
      {};

      return {
          "sendMessage" : _sendMessage,
          "sendEmail" : _sendEmail,
          "deleteMessage" : _deleteMessage,
          "getMessages" : _getMessages
      }

})();